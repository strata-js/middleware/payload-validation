# Strata Payload Validation Middleware
Payload validation middleware for use with a [Strata Service](https://gitlab.com/stratajs/strata).
See the [Strata Middleware](https://gitlab.com/stratajs/strata/-/blob/master/docs/middleware.md) documentation for more details.

Strata Payload Validation Middleware uses [AJV](https://ajv.js.org/) internally to validate payloads that are
passed to the Strata Service. 

The following [AJV Plugins](https://ajv.js.org/packages/) are included in the middleware:
 * [ajv-formats](https://ajv.js.org/packages/ajv-formats.html)
 * [ajv-errors](https://ajv.js.org/packages/ajv-errors.html)
 * [ajv-keywords](https://ajv.js.org/packages/ajv-keywords.html)

## Installation

This middleware package is published via NPM's npm repository.

```bash
// npm
$ npm add @strata-js/middleware-payload-validation

// yarn
$ yarn add @strata-js/middleware-payload-validation
```

## Usage

#### Setting up payload validation
A validation configuration must be provided that is an object with the operation name(s) as the property names and
a valid [JSON Schema](https://ajv.js.org/json-schema.html) as the value.
An optional custom error message formatter _may_ be provided that will format the error message
according to the supplied function.

```typescript
import {Context} from '@strata-js/strata';
import {PayloadValidationMiddleware} from '@strata-js/middleware-payload-validation';
import {DefinedError} from 'ajv';

const context = new Context();

// using the default error formatter
const validationConfig = {
    doSomething: {
        type: 'object',
        required: ['foo', 'bar'],
        properties: {
            foo: {type: 'string', minLength: 2, maxLength: 2, pattern: '^\\d{2}$'},
            bar: {type: 'integer', minimum: 1, maximum: 1000},
            baz: {enum: [[2, "foo", {foo: 'bar'}, [1, 2, 3]]]}
        }
    }
}
const payloadValidationMiddleware = new PayloadValidationMiddleware(validationConfig);

context.operation('doSomething', async (request): Promise<any> => {
    return this._doSomething(request.payload);
}, [payloadValidationMiddleware]);

// using the optional error formatter
const otherValidationConfig = {
    doSomethingElse: {
        type: 'object',
        required: ['foo'],
        properties: {
            foo: {type: 'string', minLength: 1}
        }
    }
}
const payloadValidationMiddlewareScrubbed = new PayloadValidationMiddleware(
    otherValidationConfig,
    (errors: DefinedError[]) =>
    {
        return 'Invalid Request: request is not in the correct format';
    }
);

context.operation('doSomethingElse', async (request): Promise<any> => {
    return this._doSomethingElse(request.payload);
}, [payloadValidationMiddlewareScrubbed]);
```

#### Future Improvements
 * Support [JSON Type Definition](https://ajv.js.org/json-type-definition.html)
 * Support [User Defined Keywords](https://ajv.js.org/keywords.html)
 * Make [AJV Plugins](https://ajv.js.org/packages/) used configurable
