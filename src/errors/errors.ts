//----------------------------------------------------------------------------------------------------------------------

import { errors } from '@strata-js/strata';

//----------------------------------------------------------------------------------------------------------------------

export class ValidationError extends errors.ServiceError
{
    code = 'VALIDATION_ERROR';
    details : unknown;

    constructor(details : unknown)
    {
        super('Schema validation failed. (See `details` property for more information.)');

        this.details = details;
    }

    toJSON() : { name : string; message : string; code : string; stack ?: string; details : unknown; }
    {
        const { name, message, code } = super.toJSON();
        return {
            name: name as string ?? 'UnknownError',
            message: message as string ?? 'An unknown error type occured.',
            code: code as string ?? 'UNKNOWN_ERROR',
            stack: undefined,
            details: this.details,
        };
    }
}

//----------------------------------------------------------------------------------------------------------------------
