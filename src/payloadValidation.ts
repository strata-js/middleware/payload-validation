//----------------------------------------------------------------------------------------------------------------------

import { Request, ResponseMessage, logging } from '@strata-js/strata';

import { Options as AJVOptions, Ajv, DefinedError } from 'ajv';
import InstallAjvErrors from 'ajv-errors';
import InstallAjvFormats from 'ajv-formats';
import InstallAjvKeywords from 'ajv-keywords';

import { AjvErrorParser, ValidationError } from './errors/index.js';

//----------------------------------------------------------------------------------------------------------------------

export type PayloadValidationSchemaConfig = Record<string, Record<string, unknown>>;

//----------------------------------------------------------------------------------------------------------------------

export class PayloadValidationMiddleware
{
    readonly #logger;
    readonly #validationSchemaConfig : PayloadValidationSchemaConfig;
    readonly #validator;
    readonly #errorFormatter;

    constructor(
        config : PayloadValidationSchemaConfig,
        ajvOptions ?: AJVOptions,
        errorFormatter ?: (errors : DefinedError[]) => any
    )
    {
        this.#logger = logging.getLogger('PayloadValidationMiddleware');
        this.#validationSchemaConfig = config;
        this.#errorFormatter = errorFormatter;

        // Create AJV validator instance and install plugins
        const ajvConfig = { ...ajvOptions, allErrors: true, $data: true, discriminator: true };
        this.#validator = new Ajv(ajvConfig);
        InstallAjvErrors.default(this.#validator);
        InstallAjvFormats.default(this.#validator);
        InstallAjvKeywords.default(this.#validator);

        // Add all schemas to AJV instance, this does not compile them,
        // but they will be compiled and cached on first use.
        Object.entries(this.#validationSchemaConfig).forEach(([ key, value ]) =>
        {
            this.#validator.addSchema(value, key);
        });
    }

    public async beforeRequest(request : Request) : Promise<Request | undefined>
    {
        const { payload, operation, context } = request;
        const validate = this.#validator.getSchema(operation);

        if(validate)
        {
            // perform actual validation of payload against the JSON schema
            const valid = validate(payload);
            if(!valid)
            {
                // parse validation errors to clean up some of the default messages
                const validationErrors = AjvErrorParser.parseErrors(validate.errors);
                request.messages?.push({
                    message: 'Schema validation failed. (See `details` property for more information.)',
                    code: 'payload_validation_error',
                    severity: 'error',
                    type: 'validation_error',
                    details: { validationErrors },
                } as ResponseMessage<{ validationErrors : string[] }>);

                let details = validationErrors;

                // format error details if a formatter was provided
                if(this.#errorFormatter)
                {
                    details = this.#errorFormatter(details);
                }
                request.fail(new ValidationError(details));
            }
        }
        else
        {
            this.#logger.debug(`operation ${ context }.${ operation } has no validation schema defined.`);
        }

        return request;
    }
}

//----------------------------------------------------------------------------------------------------------------------
